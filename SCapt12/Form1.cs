﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace SCapt12
{
    public partial class screenrecorder : Form
    {
        private int width;
        private int height;

        private int scaleX = 0;
        private int scaleY = 0;

        public float scale = 1.0f;

        int startTime;
        int recordTime;

        private Bitmap bmp;
        private List<Bitmap> bmps = new List<Bitmap>();
        private Process convert = new Process();

        int frameRate;
        SaveFileDialog sf = new SaveFileDialog();

        string dir;
        string ffmpeg;

        public screenrecorder()
        {
            preCheck();
            InitializeComponent();

            dir = Directory.GetCurrentDirectory();
            ffmpeg = dir + "\\3rd\\ffmpeg.exe";

            if (!File.Exists(ffmpeg))
            {
                MessageBox.Show("FFMPEG not found in path " + ffmpeg, "Error", MessageBoxButtons.OK);
                this.Dispose();
            }

            foreach (Screen s in Screen.AllScreens)
            {
                width += s.Bounds.Width;

                if (s.Bounds.Height > height)
                {
                    height = s.Bounds.Height;
                }
            }

            bmp = new Bitmap(width, height);

            scaleX = (int)(bmp.Width * scale);
            scaleY = (int)(bmp.Height * scale);

            sf.DefaultExt = "avi";
            sf.Filter = "MOV (*.mov)|*.mov|AVI (*.avi)|*.avi";
            sf.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            sf.FileOk += sf_FileOk;
        }

        private void preCheck()
        {
            if (!Directory.Exists("tmp"))
            {
                Directory.CreateDirectory("tmp");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("tmp");

            foreach (FileInfo file in di.GetFiles()) {
                file.Delete();
            }

            startTime = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            timer1.Start();
            timer2.Start();

            stop.Enabled = true;
            record.Enabled = false;
            export.Enabled = false;
            clear.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int stopTime = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            try
            {
                recordTime = stopTime - startTime;
            }
            catch (Exception ex)
            {
                recordTime = 1;
            }

            timer1.Stop();
            timer2.Stop();
            timer2_Tick(sender, e);

            clear.Enabled = true;
            export.Enabled = true;
            record.Enabled = true;
            stop.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.CopyFromScreen(0, 0, 0, 0, new Size(width, height));
                g.DrawImage(bmp, new Rectangle(((int)width - scaleX) / 2, ((int)height - scaleY) / 2, scaleX, scaleY));
                g.Dispose();
            }

            bmps.Add(bmp.Clone(new RectangleF(0, 0, width, height), System.Drawing.Imaging.PixelFormat.Format24bppRgb));
        }

        int tmpId = 0;

        private void timer2_Tick(object sender, EventArgs e)
        {
            List<Bitmap> tmpb = bmps;
            foreach (Bitmap b in tmpb)
            {
                b.Save("tmp/image" + tmpId + ".jpg", ImageFormat.Jpeg);
                tmpId++;
                b.Dispose();
            }

            tmpb.Clear();
            bmps.Clear();
        }

        private void export_Click(object sender, EventArgs e)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo("tmp");
                int totalFrames = di.GetFiles().Length;

                try
                {
                    frameRate = (totalFrames / recordTime);
                    sf.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Nothing to export.", "Notice", MessageBoxButtons.OK);
                    convert_Exited(sender, e);
                }
            }
            catch (Win32Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void sf_FileOk(object sender, CancelEventArgs e)
        {
            if (frameRate < 1)
            {
                frameRate = 1;
            }

            convert.StartInfo.FileName = @ffmpeg;
            convert.StartInfo.Arguments = "-f image2 -r " + frameRate + " -i tmp/image%d.jpg -b 50000000 -an " + sf.FileName;
            convert.StartInfo.CreateNoWindow = true;
            convert.EnableRaisingEvents = true;
            convert.Exited += new EventHandler(convert_Exited);
            convert.Start();

        }

        void convert_Exited(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("tmp");
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            export.Invoke(new MethodInvoker(delegate { export.Enabled = false; }));
            clear.Invoke(new MethodInvoker(delegate { clear.Enabled = false; }));
            tmpId = 0;
           
        }

        private void clear_Click(object sender, EventArgs e)
        {
            DialogResult confirm = MessageBox.Show("Are you sure?", "Clear buffer.", MessageBoxButtons.OKCancel);

            if (confirm == DialogResult.OK)
            {
                convert_Exited(sender, e);
                bmps.Clear();
                clear.Enabled = false;
                export.Enabled = false;
            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            convert_Exited(sender, e);
        }
    }
}
